<?php

function reverseString($str) {
    // Функция должна выводить последовательность символов в обратном порядке.
    // В аргумент $str может быть передана не только строка


	$chars = mb_str_split($str);

    return implode('', array_reverse($chars));

}