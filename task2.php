<?php

function createFileWithSum(string $pathToFiles): void {
    // Даны два текстовых файла 1.txt и 2.txt. Они находятся в директории $pathToFiles
    // Каждый файл содержит по n целых чисел, располагающихся на отдельных строках.
    // Необходимо вычислить суммы чисел из двух файлов на соответствующих строках и записать их в файл 3.txt.
    // Файл 3.txt необходимо создать в директории $pathToFiles

	$dir = $pathToFiles."/"; // Например /var/www/localhost/public
	if(!is_dir($dir)) {
		return ;
		mkdir($dir, 0777, true);
	    $pathToFiles="/fixtures";
	}

	$file_array1 = file($pathToFiles."/1.txt") ;
	$file_array2 = file($pathToFiles."/2.txt") ;
	$file_array3 = [];
	$file3 = fopen($pathToFiles."/3.txt", 'w');

	foreach ($file_array1 as $key => $value) {
		$file_array3[$key] = (int)$file_array1[$key]+(int)$file_array2[$key];
		if($key < count($file_array1)-1) {
		fwrite($file3, (int)$file_array3[$key].PHP_EOL);
	}
		else {
			fwrite($file3, $file_array3[$key]);
		}

	}
}