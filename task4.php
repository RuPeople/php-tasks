<?php

function createDeepArrayOfNumbers(int $deep): array {
    // Реализуйте функцию, которая случайным образом создает массив из X элементов,
    // состоящий из случайных целых чисел и из массивов целых чисел.
    // Глубина массива - $deep.
    // X должно быть больше 5 и меньше 10
    // Числа должны находиться в диапозоне от 10 до 10000
    $x = random_int(6, 9);
    $arr = array($x);

    for ($i=1;$i<$x;$i++) {
        $arr[$i] = random_int(10, 10000);
    }

    return $arr;


}

function calculateSum(array $deepArrayOfNumbers): int {
    // Напишите функцию которая вычисляет сумму чисел всех элементов и подэлементов структуры,
    // создаваемой функцией createDeepArrayOfNumbers.
    $total = 0;
    foreach(new recursiveIteratorIterator( new recursiveArrayIterator($deepArrayOfNumbers)) as $sub)
    {
        $total += (int)  $sub;
    }
    return $total;
}
